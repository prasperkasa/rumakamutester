var showLoginStatus = false;
var showPopupStatus = false;
var showPopupReviewStatus = false;
var showPopupVoucherStatus = false;
var showPopupResiStatus = false;


// GET CLASS
function getElement(attr){
  return document.querySelector(attr);
};

// GET CLASS
function getClass(attr){
  return document.getElementsByClassName(attr);
};

// GET ID
function getId(attr){
  return document.getElementById(attr);
};

// GET WINDOW HEIGHT
var windowHeight = getClass('window_height');
for (var i = 0; i < windowHeight.length; i++) {
  windowHeight[i].style.height=window.innerHeight;
};





// FLOATING MENU
function menuFixed() {
    if (document.body.scrollTop > 1 || document.documentElement.scrollTop > 1) {
        getId("body").classList.add("floating");
        
    } else {
        getId("body").classList.remove("floating");
       
    }
};


// ANIMATE VISIBLE ELEMENT
function checkVisible(elm) {
  var rect = elm.getBoundingClientRect();
  var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
  return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
};


function changeForm(self, val){
    var nav = getClass("ol-popup-box__login-link");
    var spec = getClass("ol-popup-box__login-type-form");
    for (var i = 0; i < spec.length; i++) {
        spec[i].classList.remove('show');
        nav[i].classList.remove('active');
    }
    self.classList.add('active');
    getId("login"+val).classList.add('show');
}

function changeDet(self, val){
    var nav = getClass("ol-product-content-detail__nav-link");
    var spec = getClass("ol-product-content-detail__list-item");
    for (var i = 0; i < spec.length; i++) {
        spec[i].classList.remove('show');
        nav[i].classList.remove('active');
    }
    self.classList.add('active');
    getId("detail"+val).classList.add('show');
}

function showDetail(self){
    var el = self.nextElementSibling;
    if(el.style.display == 'none' ) {
        self.children[0].innerHTML = "<i class='ol-fa fas fa-angle-up'></i>"
        self.classList.add("active");
        el.style.display = 'block';
    }else{
        self.children[0].innerHTML = "<i class='ol-fa fas fa-angle-down'></i>"
        self.classList.remove("active");
        el.style.display = 'none';
    }
}

function showmenu(self){
    var el = self.nextElementSibling;
    if(el.style.display == 'none' ) {
        self.classList.add("active");
        el.style.display = 'block';
        el.classList.add("show");
    }else{
        self.classList.remove("active");
        el.style.display = 'none';
        el.classList.remove("show");
    }
}


function showPopup(){
    if(!showPopupStatus){
        getClass("ol-popup--general")[0].classList.add("show");
        getElement("body").classList.add("popup");
        showPopupStatus=true;
    }else{
        getClass("ol-popup--general")[0].classList.remove("show");
        getElement("body").classList.remove("popup");
        showPopupStatus=false;
    }
}

function showVoucher(){
    if(!showPopupStatus){
        getClass("ol-popup--voucher")[0].classList.add("show");
        getElement("body").classList.add("popup");
        showPopupStatus=true;
    }else{
        getClass("ol-popup--voucher")[0].classList.remove("show");
        getElement("body").classList.remove("popup");
        showPopupStatus=false;
    }
}

function showPopupReview(){
    if(!showPopupReviewStatus){
        getClass("ol-popup--general")[0].classList.add("show");
        getElement("body").classList.add("popup");
        showPopupReviewStatus=true;
    }else{
        getClass("ol-popup--general")[0].classList.remove("show");
        getElement("body").classList.remove("popup");
        showPopupReviewStatus=false;
    }
}

function showPopupResi(){
    if(!showPopupResiStatus){
        getClass("ol-popup--resi")[0].classList.add("show");
        getElement("body").classList.add("popup");
        showPopupResiStatus=true;
    }else{
        getClass("ol-popup--resi")[0].classList.remove("show");
        getElement("body").classList.remove("popup");
        showPopupResiStatus=false;
    }
}

function showLogin(){
    if(!showLoginStatus){
        getClass("ol-popup--login")[0].classList.add("show");
        getElement("body").classList.add("popup");
        getClass("accordionItem")[0].className = "accordionItem close";
        showLoginStatus=true;
    }else{
        getClass("ol-popup--login")[0].classList.remove("show");
        getElement("body").classList.remove("popup");
        // getClass("accordionItem close")[0].className = "accordionItem close";
        showLoginStatus=false;
    }
}


// function cartToggle() {
//   var element = document.getElementById("cartfloating");

//   if (element.classList) { 
//     element.classList.toggle("show");
//   } else {
//     var classes = element.className.split(" ");
//     var i = classes.indexOf("show");

//     if (i >= 0) 
//       classes.splice(i, 1);
//     else 
//       classes.push("show");
//       element.className = classes.join(" "); 
//   }
// }

// function notifToggle() {
//   var element = document.getElementById("notiffloating");

//   if (element.classList) { 
//     element.classList.toggle("show");
//   } else {
//     var classes = element.className.split(" ");
//     var i = classes.indexOf("show");

//     if (i >= 0) 
//       classes.splice(i, 1);
//     else 
//       classes.push("show");
//       element.className = classes.join(" "); 
//   }
// }



//accordion
var accItem = document.getElementsByClassName('accordionItem');
var accHD = document.getElementsByClassName('accordionItemHeading');
for (i = 0; i < accHD.length; i++) {
    accHD[i].addEventListener('click', toggleItem, false);
}
function toggleItem() {
    var itemClass = this.parentNode.className;
    for (i = 0; i < accItem.length; i++) {
        accItem[i].className = 'accordionItem close';
    }
    if (itemClass == 'accordionItem close') {
        this.parentNode.className = 'accordionItem open';
    }
}





//WINDOW SCROLL EVENT
window.onscroll = function(){
    
    //MENU FIXED
    menuFixed();
    
};



//jquery

//SOFT LINK ONE PAGE
$(function() {  $('a[href*="#"]:not([href="#"])').click(function() {    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {      var target = $(this.hash);      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');      if (target.length) {        $('html, body').animate({          scrollTop: target.offset().top-70}, 1000);        return false;      }    }  });});



//img preview
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


//accordion review
(function($) {
    
  var allPanels = $('.ol-popup-box__review .ol-popup-box__review-note').hide();
    
  $('.ol-popup-box__review .ol-popup-box__review-prd-star').click(function() {
    allPanels.slideUp();
    $(this).parent().next().slideDown();
    return false;
  });

})(jQuery);


$(document).ready(function(){
    menu_mobile();
    // nav_cat();
    nav_cat_sub();
    hero_slider();
    flash_sale();
    environment();
    home_prd();
    home_prd_slide();
    home_review();
    prd_detail();
    price_slider();
    inspiration();
    notif();
    back_top();
});


function menu_mobile(){
    $('.ol-header-bar').click(function(e){
        e.preventDefault();
        if($('.ol-header-nav').hasClass('ol-header-nav--show')){
            $('.ol-header-nav').removeClass('ol-header-nav--show');
        }else{
            $('.ol-header-nav').addClass('ol-header-nav--show');
        }
    })
};


function nav_cat(){
    $('.ol-header-category').click(function(e){
        e.preventDefault();
        if($('.ol-header-category-content').hasClass('ol-header-category-content--show')){
            $('.ol-header-category-content').removeClass('ol-header-category-content--show');
            $('.ol-header-category').removeClass('ol-header-category--show');
        }else{
            $('.ol-header-category-content').addClass('ol-header-category-content--show');
            $('.ol-header-category').addClass('ol-header-category--show');
        }
    })
};


function nav_cat_sub(){
  $(".ol-header-category__nav > li > a").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).siblings(".ol-header-category__nav-sub").hide();
    } else {
      $(".ol-header-category__nav > li > a").removeClass("active");
      $(this).addClass("active");
      $(".ol-header-category__nav-sub").hide();
      $(this).siblings(".ol-header-category__nav-sub").show();
    }
  });

};


function back_top(){
    if ($('.ol-totop').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('.ol-totop').addClass('show');
                } else {
                    $('.ol-totop').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('.ol-totop').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
};


function hero_slider(){
    $('.ol-hero-slider').slick({
      slidesToScroll: 1,
        slidesToShow: 1,
        dots: true,
        autoplay:true,
        autoplaySpeed: 5000,
        arrows: true,
        pauseOnFocus: false,
    });
};

function flash_sale(){
    $('.ol-home-flash-list').slick({
      slidesToScroll: 1,
        slidesToShow: 1,
        dots: false,
        autoplay:true,
        autoplaySpeed: 5000,
        arrows: true,
        pauseOnFocus: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
};


function environment(){
    $('.ol-home-inspiration').slick({
      slidesToScroll: 1,
        slidesToShow: 1,
        dots: true,
        autoplay:false,
        autoplaySpeed: 5000,
        arrows: true,
        pauseOnFocus: false,
    });
};


function home_prd(){
    $('.ol-home-product__content').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: false,
        fade: true,
        focusOnSelect: false,
        asNavFor: '.ol-home-product__nav',
        swipe:false,
        accessibility: false,
    });

    $('.ol-home-product__nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.ol-home-product__content',
        dots: false,
        autoplay: false,
        centerMode: false,
        focusOnSelect: true,
        arrows: false,
        swipe:false,
        accessibility: false,
    });
};


function home_prd_slide(){
    $('.ol-home-product__content-item-list').slick({
      slidesToScroll: 1,
        slidesToShow: 4,
        dots: false,
        autoplay:false,
        autoplaySpeed: 5000,
        arrows: true,
        pauseOnFocus: false,
        responsive: [
            {
              breakpoint: 481,
              settings: {
                slidesToShow: 3,
              }
            },

            {
              breakpoint: 361,
              settings: {
                slidesToShow: 2,
              }
            },
         ]
    });
};

function home_review(){
    $('.ol-home-review__list').slick({
      slidesToScroll: 1,
        slidesToShow: 2,
        dots: false,
        autoplay:false,
        autoplaySpeed: 5000,
        arrows: true,
        pauseOnFocus: false,
        responsive: [
            {
              breakpoint: 481,
              settings: {
                slidesToShow: 1,
              }
            }
         ]
    });
};


function inspiration(){
    $('.ol-inspiration__content-list').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: false,
        fade: true,
        focusOnSelect: false,
        asNavFor: '.ol-inspiration__content-nav',
        swipe:false,
        accessibility: false,
    });

    $('.ol-inspiration__content-nav').slick({
        slidesToShow: 7,
        slidesToScroll: 1,
        asNavFor: '.ol-inspiration__content-list',
        dots: false,
        autoplay: false,
        centerMode: true,
        focusOnSelect: true,
        arrows: true,
        swipe:true,
        accessibility: true,
        centerPadding: '0'
    });
};


function prd_detail(){
    $('.ol-product-content-detail__images-list-single').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        asNavFor: '.ol-product-content-detail__images-list-group'
      });
      $('.ol-product-content-detail__images-list-group').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.ol-product-content-detail__images-list-single',
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '0',
      });
}

function price_slider(){
    $(function() {
        $( "#slider-range" ).slider({
          range: true,
          min: 10000,
          max: 99999999,
          values: [ 10000, 99999999 ],
          slide: function( event, ui ) {
            $( "#amount" ).val( "Harga : Rp. " + ui.values[ 0 ] + " - Rp. " + ui.values[ 1 ] );
          }
        });
        $( "#amount" ).val( "Harga : Rp. " + $( "#slider-range" ).slider( "values", 0 ) +
          " - Rp. " + $( "#slider-range" ).slider( "values", 1 ) );
    });
}


function notif(){
    setTimeout(function() {
        $('.ol-alert').fadeOut('slow');
    }, 3500); 
}