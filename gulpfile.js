/**
 * Gulp HTML Project settings.
 */

/* NPM Modules Section
--------------------------------*/
const gulp         = require("gulp");

// CSS
const autoprefixer = require('gulp-autoprefixer');
const sass         = require("gulp-sass");
const cssmin       = require("gulp-cssmin");

// JS
const uglify       = require("gulp-uglify");

// File manipulation
const concat       = require("gulp-concat");
const fileinclude  = require('gulp-file-include');

// Image optimization
const image        = require('gulp-image');

// Notification for gulp tasks
const notify       = require('gulp-notify');

// Error Notify
var plumber = require('gulp-plumber');

// Watch for file change
const watch        = require('gulp-watch');

// Live browser sync
const browsersync  = require('browser-sync').create();


const pump = require("pump");



/* Tasks Section
--------------------------------*/
// CSS
gulp.task('styles', function(){
  gulp.src('src/scss/*.scss')
  	.pipe(plumber())
    .pipe(sass())
    .pipe(plumber.stop())
    .pipe(cssmin())
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('dist/css'))
    .pipe(browsersync.reload({stream: true}))
});

// JS
// gulp.task('scripts', function (cb) {

//   // main JS
//   gulp.src('src/js/*.js')
//     // .pipe(concat('script.js'))
//     .pipe(plumber())
//     .pipe(plumber.stop())
//     .pipe(gulp.dest('dist/js'))
//     .pipe(browsersync.reload({stream: true}))

// });


// JS
gulp.task("scripts", function (cb) {
  pump([
      gulp.src([
        "src/js/script.js"
      ]),
      uglify(),
      gulp.dest("dist/js"),
      browsersync.reload({stream: true})
    ],
    cb
  );
});




// Image optimization
gulp.task('image', function () {
  gulp.src('src/images/*')
    .pipe(image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      jpegRecompress: false,
      mozjpeg: true,
      guetzli: false,
      gifsicle: true,
      svgo: true,
      concurrent: 10
    }))
    .pipe(gulp.dest('dist/images'))

});

// File Include for HTML
gulp.task('html', function() {
  gulp.src(['src/**/*.html', '!src/**/_*/**'])
    // gulp file include
    .pipe(fileinclude({
      prefix: '@@',
      basepath: './src/'
    }))
    .pipe(gulp.dest('dist/'))
});

// Watch for file change
gulp.task('watch', function() {

  gulp.watch('src/scss/**/*.scss', ['styles']).on('change', browsersync.reload);
  gulp.watch('src/js/**/*.js', ['scripts']).on('change', browsersync.reload);
  gulp.watch('src/images/*', ['image']);
  gulp.watch('src/**/*.html', ['html']).on('change', browsersync.reload);

});

// Live browser sync
gulp.task('browser-sync', ['styles'], function() {
  browsersync.init({
    server: {
      baseDir: './dist'
    }
  });
});

// Main Task
gulp.task( 'default', [
  'styles',
  'scripts',
  'image',
  'html',
  'watch',
  'browser-sync',
]);